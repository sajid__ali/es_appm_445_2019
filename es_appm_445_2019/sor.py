import numpy as np
import scipy.sparse.linalg as splinalg
import scipy.sparse as ssparse
from .utils import *

__all__ = ['sor']

'''
sor

Inputs  : A,b,w
Outputs : G,b1

A and b are from the original eq to be solved (Ax=b)
w is an additional parameter for specifying the weight
G and b1 for sor are as per 2.64
'''

def sor(A,b,w=1.8):
    a  = A.toarray()
    D  = np.diag(np.diag(a))
    Lu = -1*np.triu(a,k=1)
    Ll = -1*np.tril(a,k=-1)
    del a
    Q  = ssparse.csc_matrix(D-w*Ll)
    P  = ssparse.csc_matrix(w*Lu+(1-w)*D)
    Q_ = splinalg.inv(Q)
    G  = Q_@P
    b1 = w*Q_@b
    return G,b1
