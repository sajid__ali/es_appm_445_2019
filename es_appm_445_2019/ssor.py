import numpy as np
import scipy.sparse.linalg as splinalg
import scipy.sparse as ssparse
from .utils import *

__all__ = ['ssor']

'''
ssor

Inputs  : A,b,w
Outputs : G,b1

A and b are from the original eq to be solved (Ax=b)
w is an additional parameter for specifying the weight
K as per 2.70
b_ as per http://www.netlib.org/linalg/html_templates/node17.html
'''

def ssor(A,b,w=1.8):
    a  = A.toarray()
    D  = np.diag(np.diag(a))
    Lu = -1*np.triu(a,k=1)
    Ll = -1*np.tril(a,k=-1)
    del a
    G1 = ssparse.csc_matrix(D-w*Ll)
    G2 = ssparse.csc_matrix(w*Lu+(1-w)*D)
    G1_= splinalg.inv(G1)
    G  = G1_@G2
    H1 = ssparse.csc_matrix(D-w*Lu)
    H2 = ssparse.csc_matrix(w*Ll+(1-w)*D)
    H1_= splinalg.inv(H1)
    H  = H1_@H2
    K  = H@G 
    b_ = w*(2-w)*H1_@D@G1_@b
    return K,b_
