import numpy as np
import scipy.sparse as ssparse

__all__ = ['get_A_dirichlet',
           'get_init_dirichlet',
           'get_A_neumann',
           'get_init_neumann',
           'get_I_h_2h',
           'get_I_2h_h']

'''
get_A_dirichlet

Inputs  : n,k
Outputs : A

n defines the size of the problem
k is the parameter set to one of (0,2)
A is the matrix for dirichlet B.C.
'''
def get_A_dirichlet(n,k):
    h = 1/n
    A = (2*ssparse.eye(n) - ssparse.eye(n,k=1) - ssparse.eye(n,k=-1))*(1/(h*h))
    A += ssparse.eye(n)*k*np.pi*np.pi
    return A

'''
get_init_dirichlet

Inputs  : n
Outputs : x

n defines the size of the problem
x is the initial guess
'''
def get_init_dirichlet(n):
    x_ = np.zeros(n+2)
    for i in range(n+2):
        x_[i] = (np.sin((16*np.pi*i)/64) + np.sin((40*np.pi*i)/64))*(1/2)
    x = x_[1:-1]
    return x

'''
get_A_neumann

Inputs  : n
Outputs : A

n defines the size of the problem
A is the matrix for neumann B.C.
'''
def get_A_neumann(n):
    A = (2*ssparse.eye(n) - ssparse.eye(n,k=1) - ssparse.eye(n,k=-1))
    A[0,0]   = -1
    A[-1,-1] = -1
    A[0,1]   = 1
    A[-1,-2] = 1
    return A

'''
get_init_neumann

Inputs  : n
Outputs : x

n defines the size of the problem
x is the initial guess
'''
def get_init_neumann(n):
    x_ = np.zeros(n)
    for i in range(n):
        x_[i] = (np.sin((16*np.pi*i)/64) + np.sin((40*np.pi*i)/64))*(1/2)
    return x_

'''
get_I_h_2h

Inputs  : n1,n2
Outputs : I

n1,n2 define the grid sizes
I is the down sampling operator
'''
def get_I_h_2h(n1,n2):
    I = np.zeros((n1,n2))
    row = np.zeros(n2)
    row[:3] = np.array([1,2,1])
    for i in range(n1):
        I[i,:] = np.roll(row,2*i)
    I = I*(1/4)
    return I

'''
get_I_2h_h

Inputs  : n1,n2
Outputs : I

n1,n2 define the grid sizes
I is the up sampling operator
'''

def get_I_2h_h(n1,n2):
    I = np.zeros((n1,n2))
    col = np.zeros(n1)
    col[:3] = np.array([1,2,1])
    for i in range(n2):
        I[:,i] = np.roll(col,2*i)
    I = I*(1/2)
    return I