import numpy as np
import scipy.sparse as ssparse

__all__ = ['convert_vec_to_rb',
           'convert_vec_from_rb',
           'convert_mat_to_rb',
           'get_conversion_ordering']


'''
convert_vec_to_rb

Inputs  : x,n
Outputs : y

x is the vector in natural ordering
n defines the size of the problem
b gives the input vector in red-black ordering
'''

def convert_vec_from_rb(x,n):
    N,RB = get_conversion_ordering(n)
    y = np.zeros((n*n))
    y[N] = x[RB]
    return y

'''
convert_vec_from_rb

Inputs  : x,n
Outputs : y

x is the vector in red-black ordering
n defines the size of the problem
b gives the input vector in natural ordering
'''

def convert_vec_to_rb(x,n):
    N,RB = get_conversion_ordering(n)
    y = np.zeros((n*n))
    y[RB] = x[N]
    return y


'''
convert_mat_to_rb

Inputs  : A,n
Outputs : y

A is the matrix in natural ordering
n defines the size of the problem
b gives the input matrix in red-black ordering

The only extra logic here is the same that is
used to get the ordering of a matrix index in 1D
from rows and columns. 
'''

def convert_mat_to_rb(A,n):
    N,RB = get_conversion_ordering(n)
    N1 = N.reshape(n*n,1) + 1
    N2 = N.reshape(1,n*n) + 1
    N3 = N1@N2
    N3 = ((N1-1)*(n*n) + N2).ravel() - 1
    
    RB1 = RB.reshape(n*n,1) + 1
    RB2 = RB.reshape(1,n*n) + 1
    RB3 = RB1@RB2
    RB3 = ((RB1-1)*(n*n) + RB2).ravel() - 1
    A = A.toarray()
    A_ = np.zeros((A.shape))
    A_.ravel()[RB3] = A.ravel()[N3]
    return ssparse.csc_matrix(A_,dtype=np.float64)

'''
get_conversion_ordering

Inputs  : n
Outputs : N,RB

n defines the size of the problem
N gives the natural ordering
RB gives the red-black ordering

This function provides the ordering to be used by the 
above conversion functions. The natural ordering is
row major for 2D and the red-black ordering logic
is taken from the figure in the text. Once this logic
is used to fill the 1D array, it is reshaped to 2D array
and passed as an integer array for index based swaps.

Note that numpy allows one to copy values between arrays
using indexing. Something like A[idx1] = B[idx2] copies the 
values of B at indices given by idx2 to A at indices given 
by idx1 as long as idx1 and idx2 are integer arrays.
'''

def get_conversion_ordering(n):
    natural  = np.arange(1,(n*n)+1)
    redblack = np.zeros((n,n))
    r = np.arange(1,(n*n)/2+1)
    b = np.arange((n*n)/2+1,(n*n)+1)
    r = r.astype('int')
    b = b.astype('int')
    r_ = 0
    b_ = 0
    for i in np.arange(1,n+1):
        for j in np.arange(1,n+1):
            if (i+j+2)%2!=1:
                redblack[i-1][j-1] = r[r_]
                r_+=1
            if (i+j)%2==1:
                redblack[i-1][j-1] = b[b_]
                b_+=1
    N = (natural-1).reshape(n*n).astype('int')
    RB = (redblack-1).reshape(n*n).astype('int')
    return N,RB