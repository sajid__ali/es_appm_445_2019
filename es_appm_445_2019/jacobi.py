import numpy as np
import scipy.sparse.linalg as splinalg
import scipy.sparse as ssparse
from .utils import *

__all__ = ['jacobi']

'''
jacobi

Inputs  : A,b
Outputs : G,b1

A and b are from the original eq to be solved (Ax=b)
G and b1 for jacobi are as per 2.36
'''

def jacobi(A,b):
    a  = A.toarray()
    D  = np.diag(np.diag(a))
    Lu = -1*np.triu(a,k=1)
    Ll = -1*np.tril(a,k=-1)
    del a
    Q  = ssparse.csc_matrix(D)
    P  = ssparse.csc_matrix(Ll+Lu)
    Q_ = splinalg.inv(Q)
    G  = Q_@P
    b1 = Q_@b
    return G,b1
