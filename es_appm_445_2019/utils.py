import numpy as np
from tqdm import trange
import scipy.sparse as ssparse
import scipy.linalg as slinalg
import scipy.sparse.linalg as splinalg

__all__ = ['get_rhs',
           'get_A',
           'get_init',
           'residual',
           'iter_solve']

'''
get_rhs

Inputs  : n
Outputs : b

n defines the size of the problem
b gives the rhs in natural ordering
'''

def get_rhs(n):
    x   = np.linspace(0,1,n,dtype=np.float64)
    y   = np.linspace(0,1,n,dtype=np.float64)
    X,Y = np.meshgrid(x,y)
    Z   = 32*X*Y*(X-1)*(1-Y)
    b   = np.zeros(n*n,dtype=np.float64)
    b[:]= Z.reshape(n*n)
    del x,y,X,Y,Z
    return b


'''
get_A

Inputs  : n,k
Outputs : A

n defines the size of the problem
k defines the parameter as per the assignment
A gives the left hand side matrix in 2D
(for natural ordering)
'''
def get_A(n,k):
    h = 1/n
    a1 =  ssparse.diags(np.ones(n)*-4)  \
        + ssparse.diags(np.ones(n-1),offsets= 1)\
        + ssparse.diags(np.ones(n-1),offsets=-1)
    a1 = ssparse.kron(ssparse.eye(n),a1)*(1/(h*h))
    A = ssparse.csc_matrix(a1,dtype=np.float64)
    a_diag = np.ones(n*n,dtype='float64')*(1/(h*h))
    A += ssparse.diags(a_diag[n:],offsets= n,dtype=np.float64) 
    A += ssparse.diags(a_diag[n:],offsets=-n,dtype=np.float64) 
    A += ssparse.diags(a_diag*np.pi*np.pi*k*h*h,dtype=np.float64) 
    return A
    

'''
get_init

Inputs  : n,case
Outputs : x

n defines the size of the problem
case defines the kind of initialization (as per assignment)
x gives the inital vector in natural ordering
'''

def get_init(n,case):
    if case == 1:
        x = np.ones(n*n,dtype=np.float64)
    
    if case == 2:
        x = np.ones((n,n),dtype=np.float64)
        for i in range(n):
            for j in range(n):
                x[i][j] = (-1.0)**(i+j)
        x = x.reshape(n*n)
    if case == 3:
        x = np.ones((n,n),dtype=np.float64)
        h = 1/(n-1)
        for i in range(n):
            for j in range(n):
                x[i][j] = np.sin(np.pi*h*i)*np.sin(np.pi*h*j)
        x = x.reshape(n*n)
    return x

'''
residual

Inputs  : b,A,x
Outputs : r

A and b are from the original eq to be solved (Ax=b)
x is the solution for which residual is calculated
r is the residual as per infty norm
'''

def residual(b,A,x):
    return np.max(np.abs(b - A@x))



'''
iter_solve

Inputs  : G,b1,A,b,x,its,tol,use_tqdm
Outputs : x,r

G and b1 are as per equation 2.3 (x_(n+1) = G*x_(x)+b1)
A and b are from the original eq to be solved (Ax=b)
x holds the current solution 
its secifies many iterations to run
tol specifies tolerance at which to stop
use_tqdm specifies if progressbar & text output is needed
'''
def iter_solve(G,b1,A,b,x,its,tol,use_tqdm=True):
    r = []
    if use_tqdm:
        for i in trange(its):
            x = G@x + b1
            r.append(residual(b,A,x))
            if r[-1]<tol:
                print("converged in "+str(i)+" iterations!")
                return x,np.array(r)
    else:
        for i in range(its):
            x = G@x + b1
            r.append(residual(b,A,x))
            if r[-1]<tol:
                return x,np.array(r)
    return x,np.array(r)
