import numpy as np
import scipy.sparse.linalg as splinalg
import scipy.sparse as ssparse
from .utils import *

'''
weighted_jacobi

Inputs  : A,b,w
Outputs : G,b1

A and b are from the original eq to be solved (Ax=b)
w is an additional parameter for specifying the weight
G and b1 for jacobi are as per 2.38
'''

def weighted_jacobi(A,b,w=2/3):
    a  = A.toarray()
    D  = np.diag(np.diag(a))
    Lu = -1*np.triu(a,k=1)
    Ll = -1*np.tril(a,k=-1)
    del a
    Q  = ssparse.csc_matrix(D)
    P  = ssparse.csc_matrix((1-w)*D+w*(Ll+Lu))
    Q_ = splinalg.inv(Q)
    G  = Q_@P
    b1 = w*Q_@b
    return G,b1
