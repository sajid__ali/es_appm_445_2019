import numpy as np
import scipy.sparse as ssparse
from .cg_utils import residual
from tqdm import trange

__all__ = ['cg']


'''
cg - Conjugate Gradient

Inputs  : A,b,x,steps,tol
Outputs : x

A is the matrix for dirichlet B.C.
b is the rhs
steps is the number of steps to run
tol specifies the tolerance
x is the solution vector, updated at each iteration.
At the end of the loop, it has the solution and is returned.
'''
def cg(A,b,x_,steps,tol):
    n = len(x_)
    x = np.copy(x_)
    del x_
    r_ = []
    r = np.zeros(n)
    p = np.zeros(n)

    e = 0
    e_= 0
    r = b
    z = r
    p = z
    
    for i in trange(1,steps):
        e = np.dot(z,r)
        y = A.dot(p)
        a = e/(np.dot(p,y))
        x = x + a*p
        r = r - a*(A.dot(p))
        z = r
        if residual(b,A,x)<tol:
            return x,r_
        else:
            r_.append(residual(b,A,x))
        e_= np.dot(z,r)
        B = e_/e
        p = z + B*p
    return x,r_