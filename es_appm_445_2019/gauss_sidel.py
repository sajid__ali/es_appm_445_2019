import numpy as np
import scipy.sparse.linalg as splinalg
import scipy.sparse as ssparse
from .utils import *

__all__ = ['gauss_sidel']

'''
gauss_sidel

Inputs  : A,b
Outputs : G,b1

A and b are from the original eq to be solved (Ax=b)
G and b1 for gauss_sidel are as per 2.56
'''

def gauss_sidel(A,b):
    a  = A.toarray()
    D  = np.diag(np.diag(a))
    Lu = -1*np.triu(a,k=1)
    Ll = -1*np.tril(a,k=-1)
    del a
    Q  = ssparse.csc_matrix(D-Ll)
    P  = ssparse.csc_matrix(Lu)
    Q_ = splinalg.inv(Q)
    G  = Q_@P
    b1 = Q_@b
    return G,b1
