import numpy as np
import scipy.sparse as ssparse
from .cg_utils import residual
import scipy.sparse.linalg as splinalg
from tqdm import trange

__all__ = ['precondition_ssor',
           'pcg_ssor',
           'ssor_factor']


'''
precondition_SSOR - SSOR-Preconditioner step

Inputs  : M_,r
Outputs : M^(-1)r

M_ is the matrix used to perform
SSOR preconditioning, pre-processed avoid 
repeated computation.
r is the residual.
The solution to Mz=r is computed using
the SSOR method to one iteration.
'''
def precondition_ssor(M_,r):
    return M_.dot(r)


'''
pcg_ssor - SSOR-Preconditioned Conjugate Gradient

Inputs  : A,b,x,steps,tol
Outputs : x

A is the matrix for dirichlet B.C.
b is the rhs
steps is the number of steps to run
tol specifies the tolerance
x is the solution vector, updated at each iteration.
At the end of the loop, it has the solution and is returned.
'''
def pcg_ssor(A,b,x_,steps,tol):
    n = len(x_)
    x = np.copy(x_)
    del x_
    r_ = []
    r = np.zeros(n)
    p = np.zeros(n)

    e = 0
    e_= 0
    r = b
    M_ = ssor_factor(A,r,(2/(1+np.sin(np.pi*(2/A.shape[0])))))
    z = precondition_ssor(M_,r)
    p = z
    
    for i in trange(1,steps):
        e = np.dot(z,r)
        y = A.dot(p)
        a = e/(np.dot(p,y))
        x = x + a*p
        r = r - a*(A.dot(p))
        z = precondition_ssor(M_,r)
        if residual(b,A,x)<tol:
            return x,r_
        else:
            r_.append(residual(b,A,x))
        e_= np.dot(z,r)
        B = e_/e
        p = z + B*p
    return x,r_


'''
ssor_factor - helper routine

Inputs  : A,b,w
Outputs : M_

A is the matrix for dirichlet B.C.
b is the rhs vector.
w is the weight.
This is a helper routing to speed up 
the SSOR preconditioning step.
'''
def ssor_factor(A,b,w=1.8):
    a  = A.toarray()
    D  = np.diag(np.diag(a))
    Lu = -1*np.triu(a,k=1)
    Ll = -1*np.tril(a,k=-1)
    del a
    G1 = ssparse.csc_matrix(D-w*Ll)
    G1_= splinalg.inv(G1)
    H1 = ssparse.csc_matrix(D-w*Lu)
    H1_= splinalg.inv(H1)
    return w*(2-w)*H1_@D@G1_