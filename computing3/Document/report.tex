\documentclass[12pt]{article}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{listings}
\usepackage[section]{placeins}
\usepackage[lofdepth,lotdepth]{subfig}
\usepackage[boxruled,algoruled]{algorithm2e}

\title{ES\_APPM 445. Computing project 3}
\author{
	Sajid Ali \\
    Applied Physics\\
	Northwestern University
}

\date{\today}

\begin{document}
\maketitle



\section{Introduction}
In this report, we implement the Conjugate Gradient and Preconditioned Conjugate gradient methods and study their performance on a PDE in 1D. The name conjugate gradient arises due to the fact that these methods are variants of the gradient methods for symmetric positive definite matrices where the successive search directions are chosen to be conjugate with respect to the SPD matrix. This makes them very efficient over naive steepest descent method.

\section{Formulation of the methods }
Given a linear system we wish to solve (which perhaps arises from discretising a partial differential equation)
\begin{equation}
Ax=b
\end{equation}
We wish to solve for $x$. If the matrix $A$ happens to be symmetric positive definite, which means that the matrix is symmetic and and
\begin{equation}
(\vec{x},A\vec{x})\ge0
\end{equation}
The problem can be reformulated as finding the vector $x$ that minimizes the scalar function $\phi(x)$ given by 
\begin{equation}
\phi(\vec{x}) = \dfrac{1}{2}\vec{x}^{T}A\vec{x}-\vec{x}^{T}\vec{b}
\end{equation}
The proof for the same is given in the course notes\footnote{Bayliss-445-2017}.
\\
As descibed above, the CG and PCG methods take advatage of the fact that A is symmetric and positive definite. They ensure that with each new iteration, the minima is searched for in larger subspaces spanned by the search directions, i.e. at the $k^{th}$ iteration, the minima of $\vec{x}$ is searched from the subspace $SP(\vec{x},\vec{p_{1}},\vec{p_{2}},....\vec{p_{k}})$. The successive search directions are chosen such that they satisfy $(\vec{p_{k+1}},A\vec{p_{k}}) = 0$. The rate of convergence in the A-norm depends on to the condition number of the matrix given by 
\begin{equation}
\kappa(A) = \dfrac{\lambda_{+}}{\lambda_{-}}
\end{equation}
In general, the larger the condition number, the more spread out the eigenvalues of the matrix and the slower the convergence of CG.

Given the above fact, we modify the problem at hand to a different one given by:
\begin{equation}
M^{-1}Ax=M^{-1}b
\end{equation}
in such a way that $\kappa(M^{-1}A)\ll\kappa(A)$. We must ensure that the new matrix at hand $M^{-1}A$ is also symmetric positive definite so care must be taken while choosing a precondtioner. The naive implementation would be to just apply the CG algorithm on the new equation $\tilde{A}\tilde{x}=\tilde{b}$. But a modification of the algorithm allows us to work with the original variables instead of the new ones. We also take advatange of the fact that regular conjugate gradient is just preconditioned conjugate gradient with the preconditioner being the identity matrix.

\section{Implementation Details}
We now present pseudo-code for the implementation of PCG algorithms \footnote{All code is available at \url{https://bitbucket.org/sajid__ali/es_appm_445_2019/}}. 

\begin{algorithm}[H]
	\SetAlgoLined
	\KwResult{Find x that minimized $\phi (x)$ }
	$\vec{x}=0$\;
	$\vec{r}=\vec{b}$\;
	solve $M\vec{z}=\vec{r}$\;
	$\vec{p}=\vec{z}$\;
	$e = (\vec{z},\vec{r})$\;
	\For{m=1,N}{
		$y = A\vec{p}_{k}$\;
		$\alpha_{k} = \dfrac{e}{(\vec{p},\vec{y})}$\;
		$\vec{x} = \vec{x}+\alpha\vec{p}$\;
		$\vec{r} = \vec{r}-\alpha\vec{p}$\;
		solve\footnote{We note that while the algorithm calls for one matrix inversion per iteration (of the PCG) at the $\mbox{solve} M\vec{z}=\vec{r}$ step, this is never done in practice. We solve this equation using an relaxation method to one iteration and proceed. i.e. For each iteration of PCG  only one iteration of a relaxation method is performed.} $M\vec{z}=\vec{r}$ \;
		\eIf{converged}{
			exit loop\;
		}{
			$\beta_{k} = -\dfrac{(\vec{r},A\vec{p}_{k})}{(\vec{p},A\vec{p}_{k})}$\;
			$\vec{p}_{k+1} = \vec{r}_{k}+\beta_{k}\vec{p}_{k}$\;
		}
	}
	\caption{Preconditioned Conjugate Gradient}
\end{algorithm}


\section{Problem to solve}
The problem we solve arises from the PDE given by:
\begin{equation}
\begin{split}
-u_{xx} = f(x)
\\
u(-1) = u(1) = 0
\\
u_{exact}(x) = e^{-px^2}(1-x^{2})
\end{split}
\end{equation}

We face an unusual situation where we know the exact solution before solving the problem. This will help us estimate whether the discretization and floating point errors are reasonable and when we should stop the CG/PCG iterations.

The function $f(x)$ can be obtained by differentiating $u_{exact}$ twice.
\begin{equation}
f(x) = 2e^{-px^2}(2p^{2}(x^{2}-1)x^{2}-5px^{2}+p+1)
\end{equation}

The above equation is discretized to give the familiar linear system given by $Ax=b$, where 
\begin{equation}
A = \dfrac{1}{h^{2}}
\begin{pmatrix}
2 & -1 & \cdots & \cdots & \cdots\\
-1 & 2 & 1 & \cdots &\cdots \\
\vdots  & \ddots  & \ddots & \ddots &\vdots \\
\cdots & \cdots& -1 & 2 & -1\\
\cdots & \cdots& \cdots & -1 & 2 
\end{pmatrix}
\end{equation}
and b is given by computing $f(x)$ on the discretized grid.
\section{Convergence}
We solve the equation using a direct solver (using the spsolve function \footnote{\url{https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.spsolve.html}}) to get a numerical solution that will give us information about discretization errors. While this is an expensive mehtod it does guide us in choosing a meaningful convergence ratio. 

We know that given the numerical solution $v$ and the exact solution $u*$, we can calculate the error and the residual as:
\begin{equation}
\begin{split}
e = v - u*
\\
Ae = r
\end{split}
\end{equation}
We see from the plot below that the numerical solution gets closer to the exact solution as the sampling rate increases for $p=1$. When $p=1000$, the discretization error is so large that it makes no sense to solve the problem numerically. Hence we will only solve the $p=1000$ case for 2000 grid points. We have chosen to show the 2-norm here as it is a stricter norm when compared to the $\infty$ norm. This helps set a reasonable bound when the exact solution is rapidly varying and we want the numerical solution to match this at all points.

\begin{figure}
	\hspace{-3cm}\includegraphics[scale=0.625]{exact}
	\caption{Top, Left to Right : $p$ is set to 1 and the number of points on the grid are increased from 20 to 200 to 2000 The exact solution (known as an analytic function, in green) and direct solution (in blue) are plotted along with the residual (in magenta). Bottom, repeat the same set of plots with $p$ set to 1000.}
\end{figure}

\section{Spetral Analysis}
We now plot a histogram of the (normalized) eigenvalues for each method at each grid size. For CG, this plots the eigenvalues of $A$ but for the preconditioned methods this plots the eigenvalues of $M^{-1}A$.

We know from theory that the convergence depends on the condition number $\kappa(A)$ and the more clustered the eigenvalues are, the better the convergence will be. We see that Jacobi preconditioning doesn't change the spread, but SSOR preconditions greatly improves the clustering and thus reduces the number of iterations.

\begin{figure}[htpb!]
	\hspace{-2cm}\includegraphics[scale=0.55]{eigp1000}
	\caption{Histograms of eigenvalues corresponding to each method, $p=1000$ and grid resolution is $0.001$.}
\end{figure}


\begin{figure}
	\hspace{-0.75cm}\includegraphics[scale=0.85]{eigp1}
	\caption{Histograms of eigenvalues corresponding to each method. Top to bottom, number of points on the grid increasing from 20 to 2000 corresponding to grid resolutions of $0.1$,$0.01$,$0.001$. All cases correspind to $p=1$.}
\end{figure}



\section{Results}
\subsection{$p=1$}
\begin{figure}
	\hspace{0.5cm}\includegraphics[scale=1]{p1}
	\caption{Top to bottom, number of points on the grid increasing from 20 to 2000 corresponding to grid resolutions of $0.1$,$0.01$,$0.001$. Note that for the plots on the right, the x-axis corresponding to the number of iterations is in log-scale.}
\end{figure}



As the grid resolution increases, the numerical solution gets closer the exact analytical solution. Plain Conjugate Gradient and Jacobi-preconditioned Conjugate gradient do not show much difference in convergence rates but do take more iterations to converge as grid size increases as expected from the $O(h^{2})$ estimate. SSOR-preconditioning greatly improves convergence in all cases. Since the preconditioned method reduces the $\tilde{A}^{-1}$ norm of the residual in the transformed space (related to $\vec{\tilde{b}},\vec{\tilde{x}},{\tilde{A}}$), this makes us to not expect that the residual be monotonically decreasing and it turns out to be the case in practice.

\subsection{$p=1000$}
For the case when $p=1000$ the calcuations were only carried out for a grid size of 2000. This was the only case where the discretization error was low enough for performing the calculations to make sense. As in the previous case SSOR-preconditioning greatly improves convergence.

\begin{figure}[htpb!]
	\hspace{-1.5cm}\includegraphics[scale=0.65]{p1000}
	\caption{Calculations with $p=1000$ for a grid size of 2000. Note that for the plot on the right, the x-axis corresponding to the number of iterations is in log-scale. }
\end{figure}


\section{Conclusion}
We see from this report that care must be taken to ensure that the discretization error is small enough for the results from iterative solver to make sense. We also saw that for plain CG and preconditioned CG, the convergence depends on grid size as predicted from theory and that SSOR preconditioning works better than Jacobi preconditioning.

\pagebreak
\section{Appendix : cg\_utils.py}
\begin{lstlisting}
'''
get_A

Inputs  : N
Outputs : A

n defines the size of the problem
A is the matrix for dirichlet B.C.
'''
def get_A(N):
h = 2/N
A = (2*ssparse.eye(N) - ssparse.eye(N,k=1) - ssparse.eye(N,k=-1))*(1/(h*h))
return A


'''
get_exact_rhs

Inputs  : N,P
Outputs : x

n defines the size of the problem
p is a parameter
y is the exact rhs vector
'''
def get_exact_rhs(N,P):
x_ = np.linspace(-1,1,N+2)
y  = np.zeros(N+2)
for i in range(N+2):
x = x_[i]
y[i] = 2*np.exp(-1*P*x**2)*(2*P**2*(x**2-1)*x**2 - 5*P*x**2 + P + 1)
return y[1:-1]


'''
get_exact_sol

Inputs  : N,P
Outputs : x

n defines the size of the problem
p is a parameter
y is the exact solution
'''
def get_exact_sol(N,P):
x_ = np.linspace(-1,1,N+2)
y  = np.zeros(N+2)
for i in range(N+2):
x = x_[i]
y[i] = np.exp(-1*P*x*x)*(1 - x*x)
return y[1:-1]


'''
residual

Inputs  : b,A,x
Outputs : r

A and b are from the original eq to be solved (Ax=b)
x is the solution for which residual is calculated
r is the residual as per 2-norm
'''
def residual(b,A,x):
return np.linalg.norm((b-A.dot(x)),2)
'''
\end{lstlisting}


\section{Appendix : cg.py}
\begin{lstlisting}
'''
cg - Conjugate Gradient

Inputs  : A,b,x,steps,tol
Outputs : x

A is the matrix for dirichlet B.C.
b is the rhs
steps is the number of steps to run
tol specifies the tolerance
x is the solution vector, updated at each iteration.
At the end of the loop, it has the solution and is returned.
'''
def cg(A,b,x_,steps,tol):
n = len(x_)
x = np.copy(x_)
del x_
r_ = []
r = np.zeros(n)
p = np.zeros(n)

e = 0
e_= 0
r = b
z = r
p = z

for i in trange(1,steps):
e = np.dot(z,r)
y = A.dot(p)
a = e/(np.dot(p,y))
x = x + a*p
r = r - a*(A.dot(p))
z = r
if residual(b,A,x)<tol:
return x,r_
else:
r_.append(residual(b,A,x))
e_= np.dot(z,r)
B = e_/e
p = z + B*p
return x,r_
\end{lstlisting}

\section{Appendix : pcg\_j.py}
\begin{lstlisting}
'''
precondition_jacobi - Jacboi-Preconditioner step

Inputs  : A,r
Outputs : M^(-1)r

A is the matrix for dirichlet B.C.
r is the residual.
The solution to Mz=r is computed using
the jacobi method to one iteration.
'''
def precondition_jacobi(A,r):
D = A.diagonal()
M_ = ssparse.diags(D**(-1))
return M_.dot(r)


'''
pcg_j - Jacboi-Preconditioned Conjugate Gradient

Inputs  : A,b,x,steps,tol
Outputs : x

A is the matrix for dirichlet B.C.
b is the rhs
steps is the number of steps to run
tol specifies the tolerance
x is the solution vector, updated at each iteration.
At the end of the loop, it has the solution and is returned.
'''
def pcg_j(A,b,x_,steps,tol):
n = len(x_)
x = np.copy(x_)
del x_
r_ = []
r = np.zeros(n)
p = np.zeros(n)

e = 0
e_= 0
r = b
z = precondition_jacobi(A,r)
p = z

for i in trange(1,steps):
e = np.dot(z,r)
y = A.dot(p)
a = e/(np.dot(p,y))
x = x + a*p
r = r - a*(A.dot(p))
z = precondition_jacobi(A,r)
if residual(b,A,x)<tol:
return x,r_
else:
r_.append(residual(b,A,x))
e_= np.dot(z,r)
B = e_/e
p = z + B*p
return x,r_
\end{lstlisting}

\section{Appendix : pcg\_ssor.py}
\begin{lstlisting}
'''
precondition_SSOR - SSOR-Preconditioner step

Inputs  : M_,r
Outputs : M^(-1)r

M_ is the matrix used to perform
SSOR preconditioning, pre-processed avoid 
repeated computation.
r is the residual.
The solution to Mz=r is computed using
the SSOR method to one iteration.
'''
def precondition_ssor(M_,r):
return M_.dot(r)


'''
pcg_ssor - SSOR-Preconditioned Conjugate Gradient

Inputs  : A,b,x,steps,tol
Outputs : x

A is the matrix for dirichlet B.C.
b is the rhs
steps is the number of steps to run
tol specifies the tolerance
x is the solution vector, updated at each iteration.
At the end of the loop, it has the solution and is returned.
'''
def pcg_ssor(A,b,x_,steps,tol):
n = len(x_)
x = np.copy(x_)
del x_
r_ = []
r = np.zeros(n)
p = np.zeros(n)

e = 0
e_= 0
r = b
M_ = ssor_factor(A,r,(2/(1+np.sin(np.pi*(2/A.shape[0])))))
z = precondition_ssor(M_,r)
p = z

for i in trange(1,steps):
e = np.dot(z,r)
y = A.dot(p)
a = e/(np.dot(p,y))
x = x + a*p
r = r - a*(A.dot(p))
z = precondition_ssor(M_,r)
if residual(b,A,x)<tol:
return x,r_
else:
r_.append(residual(b,A,x))
e_= np.dot(z,r)
B = e_/e
p = z + B*p
return x,r_


'''
ssor_factor - helper routine

Inputs  : A,b,w
Outputs : M_

A is the matrix for dirichlet B.C.
b is the rhs vector.
w is the weight.
This is a helper routing to speed up 
the SSOR preconditioning step.
'''
def ssor_factor(A,b,w=1.8):
a  = A.toarray()
D  = np.diag(np.diag(a))
Lu = -1*np.triu(a,k=1)
Ll = -1*np.tril(a,k=-1)
del a
G1 = ssparse.csc_matrix(D-w*Ll)
G1_= splinalg.inv(G1)
H1 = ssparse.csc_matrix(D-w*Lu)
H1_= splinalg.inv(H1)
return w*(2-w)*H1_@D@G1_
\end{lstlisting}


\end{document}
This is never printed