import numpy as np
import scipy.sparse as ssparse
from es_appm_445_2019.utils import residual
from tqdm import trange


__all__ = ['get_A',
           'get_exact_rhs',
           'get_exact_sol',
           'residual']
           

'''
get_A

Inputs  : N
Outputs : A

n defines the size of the problem
A is the matrix for dirichlet B.C.
'''
def get_A(N):
    h = 2/N
    A = (2*ssparse.eye(N) - ssparse.eye(N,k=1) - ssparse.eye(N,k=-1))*(1/(h*h))
    return A


'''
get_exact_rhs

Inputs  : N,P
Outputs : x

n defines the size of the problem
p is a parameter
y is the exact rhs vector
'''
def get_exact_rhs(N,P):
    x_ = np.linspace(-1,1,N+2)
    y  = np.zeros(N+2)
    for i in range(N+2):
        x = x_[i]
        y[i] = 2*np.exp(-1*P*x**2)*(2*P**2*(x**2-1)*x**2 - 5*P*x**2 + P + 1)
    return y[1:-1]


'''
get_exact_sol

Inputs  : N,P
Outputs : x

n defines the size of the problem
p is a parameter
y is the exact solution
'''
def get_exact_sol(N,P):
    x_ = np.linspace(-1,1,N+2)
    y  = np.zeros(N+2)
    for i in range(N+2):
        x = x_[i]
        y[i] = np.exp(-1*P*x*x)*(1 - x*x)
    return y[1:-1]


'''
residual

Inputs  : b,A,x
Outputs : r

A and b are from the original eq to be solved (Ax=b)
x is the solution for which residual is calculated
r is the residual as per 2-norm
'''
def residual(b,A,x):
    return np.linalg.norm((b-A.dot(x)),2)