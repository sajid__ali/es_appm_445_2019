from setuptools import setup
import os,sys

if __name__ == "__main__":
    setup(name='es_appm_445_2019',
      description='Computing projects for ES-APPM-445_2019',
      url='https://bitbucket.org/sajid__ali/es_appm_445_2019/',
      author='Sajid',
      author_email='sajidsyed2021@u.northwestern.edu',
      packages=['es_appm_445_2019'])
