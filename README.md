Code for computing homeworks of course ES-APPM-445, Spring 2019 Northwestern University. Written in python 3.

Dependencies:

* Numpy
* Scipy
* Matplotlib
* h5py
* jupyter-lab
