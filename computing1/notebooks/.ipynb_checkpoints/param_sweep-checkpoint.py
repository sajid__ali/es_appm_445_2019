'''
Script to run a parameter sweep.
This stores the residues,eigenvalues and solutions at each w as a function of N,K for weighted jacobi,SOR and SSOR.
We will use these values for analyses and use the best-case w to decide on the best method for each case.
'''

'''
Import generic libraries.
'''
import numpy as np
import h5py
import os
import matplotlib.pyplot as plt
import scipy.sparse.linalg as splinalg
'''
Import iteration methods and utilities.
'''
from es_appm_445_2019.utils import get_A,get_rhs,get_init,iter_solve,residual
from es_appm_445_2019.weighted_jacobi import weighted_jacobi
from es_appm_445_2019.sor import sor
from es_appm_445_2019.ssor import ssor


'''
Create HDF5 file to store data.
'''
os.remove("param_sweep.hdf5")
f = h5py.File("param_sweep.hdf5")


'''
Create a nested for loop which iterates over method, N, K and w.
Save the following data generated for each run: eigenvalues of itearation matrix, solution, residue (at each iteartion).
'''
for meth in sor,ssor,weighted_jacobi:
    group = f.create_group(meth.__name__)
    for n in [16,32,64,128]: #problem size
        for k in [0,-1,-2]:  #param
            for w in np.linspace(0,2,100):
                init =  2  #param
                A = get_A(n,k) 
                b = get_rhs(n)
                x = get_init(n,init)
                G,b1 = meth(A,b,w)
                e_ = np.sort(np.abs(np.linalg.eigvals(G.toarray())))
                y,r = iter_solve(G,b1,A,b,x,25000,1e-7)
                
                dset1 = group.create_dataset('residue_'+str(n)+'_'+str(k)+'_'+str(w),(len(r),),dtype='f8')
                dset1[:] = r
                
                dset2 = group.create_dataset('eigvals_'+str(n)+'_'+str(k)+'_'+str(w),(n*n,),dtype='f8')
                dset2[:] = np.sort(np.abs(np.linalg.eigvals(G.toarray())))
                
                dset3 = group.create_dataset('sol_'+str(n)+'_'+str(k)+'_'+str(w),(n*n,),dtype='f8')
                dset3[:] = y
                

'''
Close the HDF5 file.
This data will be analyzed elsewhere.
'''
               
f.close()