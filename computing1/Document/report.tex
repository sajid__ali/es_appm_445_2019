\documentclass[12pt]{article}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{listings}
\usepackage[section]{placeins}
\usepackage[lofdepth,lotdepth]{subfig}

\title{ES\_APPM 445. Computing project 1}
\author{
	Sajid Ali \\
    Applied Physics\\
	Northwestern University
}

\date{\today}

\begin{document}
\maketitle



\section{Introduction}
In this report we examine the use of relaxation methods to solve Helmholtz equation. We will examine the behaviour of Jacobi, weighted-Jacobi, Guass-Sidel, red-black Gauss-Sidel symmetric Gauss-Sidel, Successive Over-Relaxation and Symmetric Successive Over-Relaxation. These methods are collectively known as relaxation methods as their update rules for each point on the grid depend only on the nearest neighbors. Moreover, all the aforementioned methods are linear, first-order, stationary methods.


\section{Formulation of the methods }
These methods can each be described either by their update rules for each point on the grid or by a collective matrix update rule. The general formulation as a matrix method is done as shown below. Given a linear system we wish to solve (which perhaps arises from discretising a partial differential equation)
\begin{equation}
Ax=b
\end{equation}
can be converted to a more computationally convinient iterative update rule 
\begin{equation}
x_{n+1} = G * x_{n}+b1
\end{equation}
The residual at each iteration is given by 
\begin{equation}
r = ||Ax-b||_{\infty}
\end{equation}
The $\infty$ norm is chosen because it gives a weak bound on the error, that is, only the largest element of the residual vector must be lower than the tolerance. If a lower norm like the 1-norm is chosen, that would require many more iterations to ensure convergence and would be very computationally demanding.
\\ \\ The $A$ matrix can be decomposed into diagonal part, lower and upper triangular matrices as per :
\begin{equation}
A = D - L - L^{T}
\end{equation}

We describe how we obtain the $G$ matrix and $b$ vector for each method below. All the equations are taken from the course notes\footnote{Bayliss-445-2017}.

\subsection{Jacobi}
The jacobi method defines the update rule to be:
\begin{equation}
\begin{split}
G  = D^{-1}*(L+L^{T})
\\
b1 = D^{-1}*b
\end{split}
\end{equation}


\subsection{Weighted-Jacobi}
The weighted-jacobi method defines the update rule to be:
\begin{equation}
\begin{split}
G  = D^{-1}*((1-w)*D+w*(L+L^{T}))
\\
b1 = D^{-1}*b*w
\end{split}
\end{equation}

\subsection{Gauss-Sidel}
The gauss-sidel method defines the update rule to be (note that the sweep is not symmetric):
\begin{equation}
\begin{split}
G  = (D-L)^{-1}*(L^{T})
\\
b1 = (D -L)^{-1}*b
\end{split}
\end{equation}

\subsection{Red-black Gauss-Sidel}
Red-black gauss-sidel is implemented by the same rule as gauss-sidel after converting the matrix A and vectors x and b to red-black ordering.

\subsection{Symmetric Gauss-Sidel}
Symmetric gauss-sidel is implemented by the ssor method with $w$ set to 1.

\subsection{Successive Over Relaxation}
The successive over relaxation method defines the update rule to be (note that the sweep is not symmetric):
\begin{equation}
\begin{split}
G  = (D-w*L)^{-1}*(w*L^{T}+(1-w)*D)
\\
b1 = (D -w*L)^{-1}*b*w
\end{split}
\end{equation}

\subsection{Symmetric Successive Over Relaxation}
The symmetric successive over relaxation method (effectively running the succesive over relaxation method twice thereby "symmetrizing" it) defines the update rule to be\footnote{\url{http://www.netlib.org/linalg/html_templates/node17.html}}:
\begin{equation}
\begin{split}
K_{w} = (D-w*L)^{-1}*(w*L^{T}+(1-w)*D)
\\
H_{w} = (D-w*L^{T})^{-1}*(w*L+(1-w)*D)
\\
G  = H_{w}*K{w}
\\
b1 = w*(2-w)*(D-w*L^{T})*D*(D-w*L)*b
\\
\end{split}
\end{equation}

\section{Implementation Details}
The code for all the methods is implemted in python and is packaged for ease of use. The source code is available at \footnote{\url{https://bitbucket.org/sajid__ali/es_appm_445_2019/}}
The structure of the code is as follows: 
\begin{itemize}
	\item \href{https://bitbucket.org/sajid__ali/es_appm_445_2019/src/master/es_appm_445_2019/utils.py}{utils.py} : utilites for running the solvers including functions that give right hand side vector, the A matrix, input data, residue calculator and iterative solve runner.
	\item \href{https://bitbucket.org/sajid__ali/es_appm_445_2019/src/master/es_appm_445_2019/red_black_utils.py}{red\_black\_utils.py} : utilities for red-black ordering including functions that convert vectors from natural ordering to red-black ordering and inverting the same, converting the A matrix from natural to red-black ordering and some functions that ease the implementation of the aforementioned functions.
	\item \href{https://bitbucket.org/sajid__ali/es_appm_445_2019/src/master/es_appm_445_2019/jacobi.py}{jacobi.py} : Function to generate the jacobi iteration matrix and modified right hand side vector.
	\item \href{https://bitbucket.org/sajid__ali/es_appm_445_2019/src/master/es_appm_445_2019/weighted_jacobi.py}{weighted\_jacobi.py} : Function to generate the weighted jacobi iteration matrix and modified right hand side vector.
	\item \href{https://bitbucket.org/sajid__ali/es_appm_445_2019/src/master/es_appm_445_2019/gauss_sidel.py}{gauss\_sidel.py} : Function to generate the gauss-sidel iteration matrix and modified right hand side vector.
	\item \href{https://bitbucket.org/sajid__ali/es_appm_445_2019/src/master/es_appm_445_2019/sor.py}{sor.py} : Function to generate the sor iteration matrix and modified right hand side vector.
	\item \href{https://bitbucket.org/sajid__ali/es_appm_445_2019/src/master/es_appm_445_2019/ssor.py}{ssor.py} : Function to generate the ssor iteration matrix and modified right hand side vector.
\end{itemize}

\section{Spectral Properties}
The spectral properties for all the iterative methods are plotted below. The upper plot for each method contains points which relates each eigenvalue to the dominant (radial) frequency associated with the corresponding eigenvector. The bottom plot is a histogram of the eigenvalues.
\begin{figure}[!htpb]
	\centering
	\hspace*{-3.5cm}\includegraphics[scale=0.675]{spectral.png}
	\caption{Top plots : Eigenvalue vs dominant (radial) frequency in eigenvector, Bottom plots : Histogram of eigenvalues. All the cases correspond to $N = 32$ and $K = 0$. }
\end{figure}

As a sanity check we see that the spectral properties of the Jacobi iteration matrix follow the behavior that is expected from class notes i.e. it has eigenvalues close to 1 at low and high frequency modes (meaning slow convergence) and eigenvalues close to 0 in between (meaning fast convergence). 
\\ In general we see that all relaxation methods damp high frequencies modes more effectively than they damp low frequency modes.\footnote{What we mean by a mode changes for each method since they all have different eigenvectors, but broadly the statement holds since we are solving the same continuous problem and expect that (at least for well posed problems) that the eigenvalues and eigenvectors of the discrete problem map to the continuous one.}

\section{Parameter Sweep}
For the methods that have a parameter associated with them viz. weighted jacobi, sor, ssor, a paramter sweep is performed to find the optimal value of the parameter. We plot the spectral radius of SOR,SSOR and weighted jacobi as a function of w for various N and K.

\begin{figure}[!htb]
\centering
	\hspace*{-3.5cm}\includegraphics[scale=0.7]{param_sweep.png}
	\caption{Plots showing the spectral radius for weighted jacobi (right y-axis), SOR and SSOR as a function of w at various combinations of N and K. The sweep was not performed for $N=64$ due to the high computional cost.}
\end{figure}

We know from theory that an estimate on the number of iterations it takes to reduce the resdual is related to the spectral radius. As we see above, for weighted jacobi, once $w$ goes beyond 1, the method does not converge. SSOR is slightly more tolerant of being away from the optimal $w$ than SOR as the change in spectral radius is less drastic. From the above, the best w values for each case were saved for later use.

\section{Dependence on K}
As $K$ changes, the spectrum of the $G$ matrix associated with each method changes thereby changing the convergence properties. For $K=2$ in particular, the problem becomes numerically unsolvable as the spectral radius for all methods exceeds 1 as is evident from the convergence plots in the next few sections.

\clearpage
\section{Initialization case 1}
Here we present the results of using each iterative method for a range of N and K when the initial vector is 1 everywhere. This corresponds to having only one low frequency in the fourier domain $(0,0)$. The residual has both high and low frequencies.
\begin{figure}[!htb]
	\centering
	\hspace*{-2cm}\includegraphics[scale=0.675]{init1.png}
	\caption{Plots showing convergence for various N and K for each method. Y-axis show the residual in log10 scale and X-axis shows the number of iterations. All methods were run to minimum of ( termination, 7500 ) iterations. The solutions for all cases where $K\neq2$ were visually inspected for correctness.}
\end{figure}
\\
We see that in general when the problem is solvable, SOR and SSOR reach the tolerance in fewest iterations and since SOR requries half the effort for one iteration when compared to SSOR, it is favourable.


\clearpage
\section{Initialization case 2}
Here we present the results of using each iterative method for a range of N and K when the initial vector is $1^{i+j}$. This corresponds to having only one high frequency in the fourier domain $(-N/2,-N/2)$. The starting residual is much higher in this case and has both high and low frequencies.
\begin{figure}[!htb]
	\centering
	\hspace*{-2cm}\includegraphics[scale=0.675]{init2.png}
	\caption{Plots showing convergence for various N and K for each method. Y-axis show the residual in log10 scale and X-axis shows the number of iterations. All methods were run to minimum of ( termination, 7500 ) iterations.The solutions for all cases where $K\neq2$ were visually inspected for correctness.}
\end{figure}
\\
We see that in general when the problem is solvable, once again SOR and SSOR reach the tolerance in fewest iterations and since SOR requries half the effort for one iteration when compared to SSOR, it is favourable. The performance benefit of adding a weight to Jacobi is also higher. We also see that the performance of red-black gauss-sidel to natural-ordering gauss-sidel is dependent on the initial condition. 

\clearpage
\section{Initialization case 3}
Here we present the results of using each iterative method for a range of N and K when the initial vector is $sin(\pi x)sin(\pi y)$. This corresponds to having both low and high frequencies. The starting residual is lowest in this case and is mainly comprised of low frequencies.
\begin{figure}[!htb]
	\centering
	\hspace*{-2cm}\includegraphics[scale=0.675]{init3.png}
	\caption{Plots showing convergence for various N and K for each method. Y-axis show the residual in log10 scale and X-axis shows the number of iterations. All methods were run to minimum of ( termination, 7500 ) iterations.The solutions for all cases where $K\neq2$ were visually inspected for correctness.}
\end{figure}
\\
We see that in general when the problem is solvable, once again SOR and SSOR reach the tolerance in fewest iterations and since SOR requries half the effort for one iteration when compared to SSOR, it is favourable.  


\section{Conclusion}
We see that the SOR method is the most efficient way to solve the Helmholtz equation. We also see that relaxation methods need more iterations to solve the same problem as the problem size increases and thus none of them are scalable.  

\section{Appendix : utils.py}
\begin{lstlisting}

'''
get_rhs

Inputs  : n
Outputs : b

n defines the size of the problem
b gives the rhs in natural ordering
'''

def get_rhs(n):
x   = np.linspace(0,1,n,dtype=np.float64)
y   = np.linspace(0,1,n,dtype=np.float64)
X,Y = np.meshgrid(x,y)
Z   = 32*X*Y*(X-1)*(1-Y)
b   = np.zeros(n*n,dtype=np.float64)
b[:]= Z.reshape(n*n)
del x,y,X,Y,Z
return b


'''
get_A

Inputs  : n,k
Outputs : A

n defines the size of the problem
k defines the parameter as per the assignment
A gives the left hand side matrix in 2D
(for natural ordering)
'''
def get_A(n,k):
h = 1/n
a1 =  ssparse.diags(np.ones(n)*-4)  \
+ ssparse.diags(np.ones(n-1),offsets= 1)\
+ ssparse.diags(np.ones(n-1),offsets=-1)
a1 = ssparse.kron(ssparse.eye(n),a1)*(1/(h*h))
A = ssparse.csc_matrix(a1,dtype=np.float64)
a_diag = np.ones(n*n,dtype='float64')*(1/(h*h))
A += ssparse.diags(a_diag[n:],offsets= n,dtype=np.float64) 
A += ssparse.diags(a_diag[n:],offsets=-n,dtype=np.float64) 
A += ssparse.diags(a_diag*np.pi*np.pi*k*h*h,dtype=np.float64) 
return A


'''
get_init

Inputs  : n,case
Outputs : x

n defines the size of the problem
case defines the kind of initialization (as per assignment)
x gives the inital vector in natural ordering
'''

def get_init(n,case):
if case == 1:
x = np.ones(n*n,dtype=np.float64)

if case == 2:
x = np.ones((n,n),dtype=np.float64)
for i in range(n):
for j in range(n):
x[i][j] = (-1.0)**(i+j)
x = x.reshape(n*n)
if case == 3:
x = np.ones((n,n),dtype=np.float64)
h = 1/(n-1)
for i in range(n):
for j in range(n):
x[i][j] = np.sin(np.pi*h*i)*np.sin(np.pi*h*j)
x = x.reshape(n*n)
return x

'''
residual

Inputs  : b,A,x
Outputs : r

A and b are from the original eq to be solved (Ax=b)
x is the solution for which residual is calculated
r is the residual as per infty norm
'''

def residual(b,A,x):
return np.max(np.abs(b - A@x))



'''
iter_solve

Inputs  : G,b1,A,b,x,its,tol,use_tqdm
Outputs : x,r

G and b1 are as per equation 2.3 (x_(n+1) = G*x_(x)+b1)
A and b are from the original eq to be solved (Ax=b)
x holds the current solution 
its secifies many iterations to run
tol specifies tolerance at which to stop
use_tqdm specifies if progressbar & text output is needed
'''
def iter_solve(G,b1,A,b,x,its,tol,use_tqdm=True):
r = []
if use_tqdm:
for i in trange(its):
x = G@x + b1
r.append(residual(b,A,x))
if r[-1]<tol:
print("converged in "+str(i)+" iterations!")
return x,np.array(r)
else:
for i in range(its):
x = G@x + b1
r.append(residual(b,A,x))
if r[-1]<tol:
return x,np.array(r)
return x,np.array(r)
\end{lstlisting}
\section{Appendix : red\_black\_utils.py}
\begin{lstlisting}
'''
convert_vec_to_rb

Inputs  : x,n
Outputs : y

x is the vector in natural ordering
n defines the size of the problem
b gives the input vector in red-black ordering
'''

def convert_vec_from_rb(x,n):
N,RB = get_conversion_ordering(n)
y = np.zeros((n*n))
y[N] = x[RB]
return y

'''
convert_vec_from_rb

Inputs  : x,n
Outputs : y

x is the vector in red-black ordering
n defines the size of the problem
b gives the input vector in natural ordering
'''

def convert_vec_to_rb(x,n):
N,RB = get_conversion_ordering(n)
y = np.zeros((n*n))
y[RB] = x[N]
return y


'''
convert_mat_to_rb

Inputs  : A,n
Outputs : y

A is the matrix in natural ordering
n defines the size of the problem
b gives the input matrix in red-black ordering

The only extra logic here is the same that is
used to get the ordering of a matrix index in 1D
from rows and columns. 
'''

def convert_mat_to_rb(A,n):
N,RB = get_conversion_ordering(n)
N1 = N.reshape(n*n,1) + 1
N2 = N.reshape(1,n*n) + 1
N3 = N1@N2
N3 = ((N1-1)*(n*n) + N2).ravel() - 1

RB1 = RB.reshape(n*n,1) + 1
RB2 = RB.reshape(1,n*n) + 1
RB3 = RB1@RB2
RB3 = ((RB1-1)*(n*n) + RB2).ravel() - 1
A = A.toarray()
A_ = np.zeros((A.shape))
A_.ravel()[RB3] = A.ravel()[N3]
return ssparse.csc_matrix(A_,dtype=np.float64)

'''
get_conversion_ordering

Inputs  : n
Outputs : N,RB

n defines the size of the problem
N gives the natural ordering
RB gives the red-black ordering

This function provides the ordering to be used by the 
above conversion functions. The natural ordering is
row major for 2D and the red-black ordering logic
is taken from the figure in the text. Once this logic
is used to fill the 1D array, it is reshaped to 2D array
and passed as an integer array for index based swaps.

Note that numpy allows one to copy values between arrays
using indexing. Something like A[idx1] = B[idx2] copies the 
values of B at indices given by idx2 to A at indices given 
by idx1 as long as idx1 and idx2 are integer arrays.
'''

def get_conversion_ordering(n):
natural  = np.arange(1,(n*n)+1)
redblack = np.zeros((n,n))
r = np.arange(1,(n*n)/2+1)
b = np.arange((n*n)/2+1,(n*n)+1)
r = r.astype('int')
b = b.astype('int')
r_ = 0
b_ = 0
for i in np.arange(1,n+1):
for j in np.arange(1,n+1):
if (i+j+2)%2!=1:
redblack[i-1][j-1] = r[r_]
r_+=1
if (i+j)%2==1:
redblack[i-1][j-1] = b[b_]
b_+=1
N = (natural-1).reshape(n*n).astype('int')
RB = (redblack-1).reshape(n*n).astype('int')
return N,RB
\end{lstlisting}
\section{Appendix : jacobi.py}
\begin{lstlisting}
'''
jacobi

Inputs  : A,b
Outputs : G,b1

A and b are from the original eq to be solved (Ax=b)
G and b1 for jacobi are as per 2.36
'''

def jacobi(A,b):
a  = A.toarray()
D  = np.diag(np.diag(a))
Lu = -1*np.triu(a,k=1)
Ll = -1*np.tril(a,k=-1)
del a
Q  = ssparse.csc_matrix(D)
P  = ssparse.csc_matrix(Ll+Lu)
Q_ = splinalg.inv(Q)
G  = Q_@P
b1 = Q_@b
return G,b1

\end{lstlisting}
\section{Appendix : weighted\_jacobi.py}
\begin{lstlisting}
'''
weighted_jacobi

Inputs  : A,b,w
Outputs : G,b1

A and b are from the original eq to be solved (Ax=b)
w is an additional parameter for specifying the weight
G and b1 for jacobi are as per 2.38
'''

def weighted_jacobi(A,b,w=2/3):
a  = A.toarray()
D  = np.diag(np.diag(a))
Lu = -1*np.triu(a,k=1)
Ll = -1*np.tril(a,k=-1)
del a
Q  = ssparse.csc_matrix(D)
P  = ssparse.csc_matrix((1-w)*D+w*(Ll+Lu))
Q_ = splinalg.inv(Q)
G  = Q_@P
b1 = w*Q_@b
return G,b1
\end{lstlisting}
\section{Appendix : gauss\_sidel.py}
\begin{lstlisting}
'''
gauss_sidel

Inputs  : A,b
Outputs : G,b1

A and b are from the original eq to be solved (Ax=b)
G and b1 for gauss_sidel are as per 2.56
'''

def gauss_sidel(A,b):
a  = A.toarray()
D  = np.diag(np.diag(a))
Lu = -1*np.triu(a,k=1)
Ll = -1*np.tril(a,k=-1)
del a
Q  = ssparse.csc_matrix(D-Ll)
P  = ssparse.csc_matrix(Lu)
Q_ = splinalg.inv(Q)
G  = Q_@P
b1 = Q_@b
return G,b1
\end{lstlisting}
\section{Appendix : sor.py}
\begin{lstlisting}
'''
sor

Inputs  : A,b,w
Outputs : G,b1

A and b are from the original eq to be solved (Ax=b)
w is an additional parameter for specifying the weight
G and b1 for sor are as per 2.64
'''

def sor(A,b,w=1.8):
a  = A.toarray()
D  = np.diag(np.diag(a))
Lu = -1*np.triu(a,k=1)
Ll = -1*np.tril(a,k=-1)
del a
Q  = ssparse.csc_matrix(D-w*Ll)
P  = ssparse.csc_matrix(w*Lu+(1-w)*D)
Q_ = splinalg.inv(Q)
G  = Q_@P
b1 = w*Q_@b
return G,b1
\end{lstlisting}
\section{Appendix : ssor.py}
\begin{lstlisting}
'''
ssor

Inputs  : A,b,w
Outputs : G,b1

A and b are from the original eq to be solved (Ax=b)
w is an additional parameter for specifying the weight
K as per 2.70
b_ as per http://www.netlib.org/linalg/html_templates/node17.html
'''

def ssor(A,b,w=1.8):
a  = A.toarray()
D  = np.diag(np.diag(a))
Lu = -1*np.triu(a,k=1)
Ll = -1*np.tril(a,k=-1)
del a
G1 = ssparse.csc_matrix(D-w*Ll)
G2 = ssparse.csc_matrix(w*Lu+(1-w)*D)
G1_= splinalg.inv(G1)
G  = G1_@G2
H1 = ssparse.csc_matrix(D-w*Lu)
H2 = ssparse.csc_matrix(w*Ll+(1-w)*D)
H1_= splinalg.inv(H1)
H  = H1_@H2
K  = H@G 
b_ = w*(2-w)*H1_@D@G1_@b
return K,b_
\end{lstlisting}

\end{document}
This is never printed