\documentclass[12pt]{article}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{listings}
\usepackage[section]{placeins}
\usepackage[lofdepth,lotdepth]{subfig}

\title{ES\_APPM 445. Computing project 2}
\author{
	Sajid Ali \\
    Applied Physics\\
	Northwestern University
}

\date{\today}

\begin{document}
\maketitle

\section{Introduction}
In this report, the following PDE will be solved using the multigrid scheme :
\begin{equation}
-u_{xx}+K\pi ^{2}u = 0
\end{equation}
with both Dirichlet and Neumann boundary conditions for $K=0$ and $K=2$.
\section{Multigrid}
Multigrid schemes arise out of the insight that relaxation methods are more effective at reducing high frequency residuals than they are at reducing low frequency residuals. This insight is used to design an iteration scheme involing fine and coarse grids that takes advantage of the high-frequency performance of relaxation methods. In the V-cycle iteration scheme implemented in this report, the high frequency residual are eliminated using a relaxation scheme like weighted-jacobi on the fine grid, leaving the low-frequency residual which is transferred on the coarse grid where it now appears as a high-frequency residual. On the coarse grid, the error is now obtained using the residual transferred from the coarse grid. Finally the error is transferred back to the fine grid and used to obtain the final solution. 
\section{Dirichlet BC}
\subsection{Analytic solution}
The equation we solve is : 
\begin{equation}
-u_{xx}+K\pi ^{2}u = 0
\end{equation}
with the boundaries fixed such that $u(0)=0$ and $u(1)=0$. 

While the general solution to the above equation would take the form : 
\begin{equation}
u(x) = c_1*e^{\pi\sqrt{K}x} + c_2*e^{-\pi\sqrt{K}x} + c_3
\end{equation}
where $c_1,c_2,c_3$ represent some constants to be determined by the boundary value constraints. 

The only values of $c_1,c_2,c_3$ for which the solution is zero at both the boundaries is $0,0,0$ thereby meaning that the only solution is zero everywhere.

Converting the continous problem to the standard discretization, the only solution that satisfies the equation will be a null-vector.

In discrete form the equation to solve becomes $Ax=b$, where $b$ is the null vector and A is given by : 

$$
A = (1/h^2)*
\begin{bmatrix}
2 +k & -1 & \cdots &\cdots &\cdots &\cdots\\
-1 & 2+k & -1 & \cdots &\cdots &\cdots\\
\cdots & -1 & 2+k & -1 & \cdots &\cdots\\
\vdots  & \ddots  & \ddots &\ddots & \vdots &\vdots \\
\vdots& \vdots  & \ddots  & \ddots &\ddots & \vdots  \\
\cdots &\cdots&\cdots -1 & 2+k & -1 &\cdots\\
\cdots &\cdots&\cdots &\cdots -1 & 2+k & -1 \\
\cdots&\cdots &\cdots&\cdots &\cdots -1 & 2+k\\
\end{bmatrix}
$$ 
with $k = K\pi^{2}h^{2}$ and h is the grid spacing.
\subsection{Briggs Multigrid plots}
Below we reproduce the plot from the Briggs multigrid tutorial. The initial guess is :
\begin{equation}
u(x) = \dfrac{1}{2}\Big[sin(\dfrac{16j\pi}{n})+sin(\dfrac{40j\pi}{n})\Big]
\end{equation}
\newpage
\subsubsection{$K=0$}
The equation we solve is : 
\begin{equation}
-u_{xx} = 0
\end{equation}
with the boundaries fixed such that $u(0)=0$ and $u(1)=0$. 

\begin{figure}[!htpb]
	\centering
	\hspace*{-3.5cm}\includegraphics[scale=0.6]{dirichlet_0.png}
	\caption{The six plots from Birggs for $K=0$ : initial guess, one sweep on weighted-jacobi on fine grid, three sweeps of weighted-jacobi on fine grid, three sweeps of weighted-jacobi on fine grid followed by one coarse sweep, three sweeps of weighted-jacobi on fine grid followed by three coarse sweeps, three sweeps of weighted-jacobi on fine grid followed by three coarse sweeps followed by three fine sweeps.}
\end{figure}

\newpage
\subsubsection{$K=2$}
The equation we solve is : 
\begin{equation}
-u_{xx} + 2\pi ^{2}u= 0
\end{equation}
with the boundaries fixed such that $u(0)=0$ and $u(1)=0$. 

\begin{figure}[!htpb]
	\centering
	\hspace*{-3.5cm}\includegraphics[scale=0.6]{dirichlet_2.png}
	\caption{The six plots from Birggs for $K=2$ : initial guess, one sweep on weighted-jacobi on fine grid, three sweeps of weighted-jacobi on fine grid, three sweeps of weighted-jacobi on fine grid followed by one coarse sweep, three sweeps of weighted-jacobi on fine grid followed by three coarse sweeps, three sweeps of weighted-jacobi on fine grid followed by three coarse sweeps followed by three fine sweeps.}
\end{figure}

With $K=2$, the system becomes more diagonally dominant and better convergence is observed.

\newpage
\section{Neumann BC}
\subsection{Analytic solution}
The equation we solve is : 
\begin{equation}
-u_{xx}+K\pi ^{2}u = 0
\end{equation}
with the boundaries fixed such that $u^{'}(0)=0$ and $u^{'}(1)=0$. 

While the general solution to the above equation would take the form : 
\begin{equation}
u(x) = c_1*e^{\pi\sqrt{K}x} + c_2*e^{-\pi\sqrt{K}x} + c_3
\end{equation}
where $c_1,c_2,c_3$ represent some constants to be determined by the boundary value constraints. 
Now, $u^{'}(x)$ becomes : 
\begin{equation}
u^{'}(x) = c_1*\pi\sqrt{K}*e^{\pi\sqrt{K}x} - c_2*\pi\sqrt{K}*e^{-\pi\sqrt{K}x}
\end{equation}
Again, the only solution is for $u^{'}(x)=0$ at the boundaries is to have $c_1,c_2$ be zero and the solution to the PDE becomes $u(x) = c$ where $c$ is an arbitray constant.

\subsection{Modifications}
We would have to take some modifications to deal with Neumann BC's. Let's first deal with the end-points themselves. For the Dirichlet case we had N+1 points going from 0 to 1 with indexing going from 0 to N, but since the boundaries were held at constant value of 0, we had a vector of size N-1. Now we have to include the end-points as well (both in the coarse and fine grids).

With central differences, we have at the left end point (similar conditions apply at the other end):
\begin{equation}
\begin{split}
u^{'}(0) = \dfrac{u(1) - u(-1)}{2h} = 0
\\
u^{'}(-1) = -u^{'}(1)
\end{split}
\end{equation}

Taking this into account, we have a vector of size N+1 and the matrix we have at hand becomes : 
$$
A = (1/h^2)*
\begin{bmatrix}
	2 & -2 & \cdots \\
	-1 & 2 & -1 & \cdots \\
	\vdots  & \vdots  & \ddots & \vdots  \\
	\cdots 	&\cdots -1 & 2 & -1 \\
	\cdots 	& \cdots & -2 & 2 \\
\end{bmatrix}
$$
which has a null space of vectors that are of the form $c*(1\cdots1)$.

Hence, we convert this to the new equation, $A^{'}x=b$ where $A^{'}$ now becomes : 

$$
A^{'} = (1/h^2)*
\begin{bmatrix}
1 & -1 & \cdots \\
-1 & 2 & -1 & \cdots \\
\vdots  & \vdots  & \ddots & \vdots  \\
\cdots 	&\cdots -1 & 2 & -1 \\
\cdots 	& \cdots & -1 & 1 \\
\end{bmatrix}
$$ 
and the first and last rows of $b$ are also divided by $2$, but since in our case, we have the null vector for b, so this operation doesn't change $b$.

We would also have to remove the zero-frequency component from the solution to ensure that the problem always converges to the one specified by $c=0$. Since there are infinitely many solutions, this has to be done to ensure uniqueness of solution.

For the dirichlet case, we had say for N1 = 8 and N2 = 4 the fine grid having points from 0 to 8 and coarse one from 0 to 4. But the end-points at coarse and fine grids were set to 0 and we took the problem size to by 7 and 3 respectively. 

For the neumann case, we again have the fine grid having points from 0 to 8 and coarse one from 0 to 4. But the end-points at coarse and fine grids need to be taken into account and this changes the transfer matrix. 

\begin{figure}[!htpb]
	\centering
	\hspace*{-3.5cm}\includegraphics[scale=0.6]{neumann.png}
	\caption{The six plots from Birggs for neumann BC : initial guess, one sweep on weighted-jacobi on fine grid, three sweeps of weighted-jacobi on fine grid, three sweeps of weighted-jacobi on fine grid followed by one coarse sweep, three sweeps of weighted-jacobi on fine grid followed by three coarse sweeps, three sweeps of weighted-jacobi on fine grid followed by three coarse sweeps followed by three fine sweeps.}
\end{figure}


\section{Appendix : mg\_utils.py}
\begin{lstlisting}
import numpy as np
import scipy.sparse as ssparse

__all__ = ['get_A_dirichlet',
'get_init_dirichlet',
'get_A_neumann',
'get_init_neumann',
'get_I_h_2h',
'get_I_2h_h']

'''
get_A_dirichlet

Inputs  : n,k
Outputs : A

n defines the size of the problem
k is the parameter set to one of (0,2)
A is the matrix for dirichlet B.C.
'''
def get_A_dirichlet(n,k):
h = 1/n
A = (2*ssparse.eye(n) - ssparse.eye(n,k=1) - ssparse.eye(n,k=-1))*(1/(h*h))
A += ssparse.eye(n)*k*np.pi*np.pi
return A

'''
get_init_dirichlet

Inputs  : n
Outputs : x

n defines the size of the problem
x is the initial guess
'''
def get_init_dirichlet(n):
x_ = np.zeros(n+2)
for i in range(n+2):
x_[i] = (np.sin((16*np.pi*i)/64) + np.sin((40*np.pi*i)/64))*(1/2)
x = x_[1:-1]
return x

'''
get_A_neumann

Inputs  : n
Outputs : A

n defines the size of the problem
A is the matrix for neumann B.C.
'''
def get_A_neumann(n):
A = (2*ssparse.eye(n) - ssparse.eye(n,k=1) - ssparse.eye(n,k=-1))
A[0,0]   = -1
A[-1,-1] = -1
A[0,1]   = 1
A[-1,-2] = 1
return A

'''
get_init_neumann

Inputs  : n
Outputs : x

n defines the size of the problem
x is the initial guess
'''
def get_init_neumann(n):
x_ = np.zeros(n)
for i in range(n):
x_[i] = (np.sin((16*np.pi*i)/64) + np.sin((40*np.pi*i)/64))*(1/2)
return x_

'''
get_I_h_2h

Inputs  : n1,n2
Outputs : I

n1,n2 define the grid sizes
I is the down sampling operator
'''
def get_I_h_2h(n1,n2):
I = np.zeros((n1,n2))
row = np.zeros(n2)
row[:3] = np.array([1,2,1])
for i in range(n1):
I[i,:] = np.roll(row,2*i)
I = I*(1/4)
return I

'''
get_I_2h_h

Inputs  : n1,n2
Outputs : I

n1,n2 define the grid sizes
I is the up sampling operator
'''

def get_I_2h_h(n1,n2):
I = np.zeros((n1,n2))
col = np.zeros(n1)
col[:3] = np.array([1,2,1])
for i in range(n2):
I[:,i] = np.roll(col,2*i)
I = I*(1/2)
return I
\end{lstlisting}

\end{document}
This is never printed